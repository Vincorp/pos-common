package com.vretail.poscommon.config;

import com.vretail.poscommon.util.VLogger;

/**
 * Created on 1/31/17.
 *
 * @author felixsoewito
 */
public enum VEnv {

    V_COMMON_DOMAIN("V_DOMAIN", "POS_COMMON");

    public String key;
    public String value;

    VEnv(String key, String defaultValue) {
        this.key = key;
        String envVal = System.getenv(key);

        if (envVal != null) {
            VLogger.info("SETUP", new Object(),"Key(" + key + ")  => " + envVal);
            this.value = envVal;
        } else {
            VLogger.warn("SETUP", new Object(),"!!! Key(" + key + ") => " + defaultValue + " (Default)");
            this.value = defaultValue;
        }
    }
}

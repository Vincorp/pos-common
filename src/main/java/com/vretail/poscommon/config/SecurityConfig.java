package com.vretail.poscommon.config;

/**
 * Created on 1/31/17.
 *
 * @author felixsoewito
 */
public class SecurityConfig {

    private static final String PRIVATE_KEY =
            "-----BEGIN RSA PRIVATE KEY-----\n"+
            "MIICXQIBAAKBgQCKjEU1K5YcRHTmgCcFMwEDM61vLfC+TCCVtkPxQ7Sd/5Rdl0W+\n"+
            "hMLHfIcop++FeQrrIXY9aJjpL82oZstaeHS03XfrmCeo10lPBpxBtF1o83RpCgWr\n"+
            "7c33hsvX37kDHT9thoYJJb9tQxGMYdR3mgxJUYWeLo8TOPfunVsqE+PHrwIDAQAB\n"+
            "AoGAXlHwK6ponwXbr7A+4ENAxTVBNRdJdcPNyx4yUuWM6KM+FhkN2g33rGcSfJMX\n"+
            "dP5aeIhnPpcewmFQ2PxiJE0s5ZMnSF2ZfmSs2m+g9kMGRVIO4ZzDmSHkqt+9P3L1\n"+
            "4dKQpXr7V9wmi+ziCq2wpVDiDzrx/olBGJyy5rxJGzexwIECQQDM/ozcOchJMNav\n"+
            "+uQUdWJ4jJX24MTNcsW6aheLrsLlrhT5a1aLV8WCujQnHH5QARC+XHGWPsY6Ak5e\n"+
            "9bqii/uHAkEArQVOu14h7mGNbY/vTBuSe1cp5ujrgGXFTgK8s2U/qeXCy56DpcCF\n"+
            "zzmxpku3uzKmnv8qJLuE30/xZ5JsRVbsmQJAUxI8huorje1qsrnbP6cqFmvyQa9o\n"+
            "UVFXa2mezQ4vEOBww3qKAcG52nYWuFrFnSoxXNMdfKNHtS5GEBlIXqKXdQJBAKyt\n"+
            "cmE/Vj/yLlMdZqcfkXb1jAU/iuIvL9Zmluu37ezjOSE8x4iidylVHD6qrOjwaSif\n"+
            "4vCcEcVJpNVIZjRhx2kCQQDGUyUZ/g7YSkmfwguX4o81AOe7QbNdVYIPydPWdjv2\n"+
            "yTEJ57Qf1cs7cJt78hvvms/99wvFkZpPdleOQAxi9F98\n"+
            "-----END RSA PRIVATE KEY-----";

    public static String getPrivateKey() {
        return PRIVATE_KEY;
    }
}

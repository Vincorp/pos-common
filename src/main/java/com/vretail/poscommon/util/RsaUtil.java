package com.vretail.poscommon.util;

import com.vretail.poscommon.config.SecurityConfig;
import com.vretail.poscommon.config.VEnv;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.Cipher;
import java.io.BufferedReader;
import java.io.FileReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created on 1/31/17.
 *
 * @author felixsoewito
 */
public class RsaUtil {

    private static final String ALGORITHM = "RSA";
    private static final String PROVIDER = "BC";

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static String getAlgorithm() {
        return ALGORITHM;
    }

    public static String encode(byte[] data) {
        return new String(Base64.encode(data));
    }

    public static byte[] decode(String data) {
        return Base64.decode(data);
    }

    public static String decrypt(String text) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM, PROVIDER);

            final String plainKey = SecurityConfig.getPrivateKey()
                    .replace("-----BEGIN RSA PRIVATE KEY-----\n", "")
                    .replace("-----END RSA PRIVATE KEY-----", "");

            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(decode(plainKey));
            final PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
            return decrypt(decode(text), privateKey);
        }catch (Exception e) {
            VLogger.error(VEnv.V_COMMON_DOMAIN.value, e, e.getMessage());
            return "";
        }
    }

    public static String decrypt(byte[] text, PrivateKey key) throws Exception {
        final Cipher cipher = Cipher.getInstance(ALGORITHM, PROVIDER);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(text));
    }

    public static String encrypt(String text) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM, PROVIDER);

            FileReader fr = new FileReader("/Users/felixsoewito/secret/pub_key.key");
            BufferedReader br = new BufferedReader(fr);
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                sb.append(line).append('\n');
            }

            final String plainKey = sb.toString()
                    .replace("-----BEGIN PUBLIC KEY-----\n", "")
                    .replace("-----END PUBLIC KEY-----", "");

            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(decode(plainKey));
            final PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
            return encrypt(text, publicKey);
        } catch (Exception e) {
            VLogger.error(VEnv.V_COMMON_DOMAIN.value, e, e.getMessage());
            return "";
        }
    }

    public static String encrypt(String text, PublicKey key) throws Exception {
        final Cipher cipher = Cipher.getInstance(ALGORITHM, PROVIDER);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return encode(cipher.doFinal(text.getBytes()));
    }

    public static void main(String[] args) {
        String hello = "192.168.43.18";

        String encrypted = encrypt(hello);
        System.out.println(encrypted);

        String decrypted = decrypt(encrypted);
        System.out.println(decrypted);

    }

}

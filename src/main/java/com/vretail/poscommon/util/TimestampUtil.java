package com.vretail.poscommon.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimestampUtil {

    private static final String TIME_STAMP_FORMAT = "yyMMddHHmmssSSS";
    private static final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    public static final SimpleDateFormat TIME_STAMP_FORMATTER = new SimpleDateFormat(TIME_STAMP_FORMAT);
    public static final SimpleDateFormat ISO8601_FORMATTER = new SimpleDateFormat(ISO8601_FORMAT);

    public static long getTimeMillis(){
        return Calendar.getInstance().getTimeInMillis();
    }

    public static String generateTimestamp() {
        return TIME_STAMP_FORMATTER.format(Calendar.getInstance().getTime());
    }

    public static String generateIso8601() {
        return ISO8601_FORMATTER.format(Calendar.getInstance().getTime());
    }
}
package com.vretail.poscommon.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 1/25/17.
 *
 * @author felixsoewito
 */
public class VLogger {

    public static final Logger LOGGER = LoggerFactory.getLogger(VLogger.class);

    public static void info(String domain, Object object, String message) {
        String logMessage = getLogMessage(domain, object, message);
        log(logMessage, Type.INFO);
    }

    public static void warn(String domain, Object object, String message) {
        String logMessage = getLogMessage(domain, object, message);
        log(logMessage, Type.WARN);
    }

    public static void error(String domain, Object object, String message) {
        String logMessage = getLogMessage(domain, object, message);
        log(logMessage, Type.ERROR);
    }

    private static String getLogMessage(String domain, Object object,
                                        String message
    ) {
        String currentTimeStamp = TimestampUtil.generateIso8601();
        String methodName = "Unknown";

        try {
            methodName = object.getClass().getEnclosingMethod().getName();
        } catch (Exception e) {
        }

        return currentTimeStamp + "|" + domain + "|"
                + methodName + "|" + message;
    }

    private static void log(String message, Type type) {
        switch (type) {
            case INFO:
                LOGGER.info(message);
                break;
            case WARN:
                LOGGER.warn(message);
                break;
            default:
                LOGGER.error(message);
                break;
        }
    }

    private enum Type {
        INFO,
        WARN,
        ERROR
    }
}
